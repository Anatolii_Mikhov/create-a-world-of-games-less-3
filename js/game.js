var canvas = document.getElementById('game');
var context = canvas.getContext('2d');
var gameStarted = false;
var friction = 0.8;
var gravity = 0.9;
var completed = false;

var door_image = new Image();
door_image.src = "images/door.png";

var player_image = new Image();
player_image.src = "images/man.png";

var keys = [];

var player = {
    x: 5,
    y: canvas.height - 30,
    width: 42,
    height: 40,
    speed: 5,
    velX: 0,
    velY: 0,
    grounded: false,
    jumping: false,
    jumpStrenght: 7,
    color: "#00ff00",
    position: 'idle',
    draw: function () {
        var startX = 42;
        if (this.position === 'left') {
            startX = 0;
        } else if (this.position === 'right') {
            startX = 84;
        }
        context.drawImage(player_image, startX, 0, 42, 40, this.x, this.y, 42, 40);
    }
};

var goal = {
    x: canvas.width - 90,
    y: 5,
    height: 51,
    width: 37,
    color: '#00259f',
    draw: function () {
        context.drawImage(door_image, this.x, this.y);
    }
};
// Platform variables
var platforms = [];
var platform_width = 120;
var platform_height = 10;

// My platform
platforms.push({
    x: 50,
    y: canvas.height - 50,
    width: platform_width,
    height: platform_height
});

platforms.push({
    x: canvas.width / 2 - platform_width / 2,
    y: canvas.height - 60,
    width: platform_width,
    height: platform_height
});

platforms.push({
    x: canvas.width - 150,
    y: canvas.height - 80,
    width: platform_width,
    height: platform_height
});

platforms.push({
    x: 80,
    y: canvas.height - 220,
    width: platform_width,
    height: platform_height
});

platforms.push({
    x: canvas.width / 2 - platform_width / 4,
    // x: canvas.width / 2,
    y: canvas.height / 2,
    width: platform_width,
    height: platform_height
});

platforms.push({
    x: canvas.width / 2,
    y: 100,
    width: platform_width,
    height: platform_height
});

platforms.push({
    x: canvas.width - 160,
    y: 55,
    width: platform_width,
    height: platform_height
});

// Lower platform
platforms.push({
    x: 0,
    y: canvas.height - 5,
    width: canvas.width,
    height: 10
});

// Walls left & right
platforms.push({
    x: -10,
    y: 0,
    width: 10,
    height: canvas.height
});

platforms.push({
    x: canvas.width,
    y: 0,
    width: 10,
    height: canvas.height
});
// Ceiling
platforms.push({
    x: 0,
    y: -10,
    width: canvas.width,
    height: 10
});
// Listen to the event, the key is pressed
document.body.addEventListener('keydown', function (event) {
    if (event.keyCode === 13 && !gameStarted) {
        startGame();
    }

    if (event.keyCode === 13 && completed) {
        reset()
    }

    keys[event.keyCode] = true;
});

document.body.addEventListener('keyup', function (event) {
    keys[event.keyCode] = false;
});

intro_screen();

function intro_screen() {
    // Name of the game
    context.font = '50px Lato';
    context.fillStyle = "#cc2366";
    context.textAlign = "center";
    context.fillText("HTML5 Game", canvas.width / 2, canvas.height / 2);
// Invitation to the game
    context.font = '20px Lato';
    context.fillText("Press Enter To Start", canvas.width / 2, canvas.height / 2 + 50);
    context.font = '12px Lato';
    context.fillStyle = "#000000";
    context.fillText("Разработано: Михов Анатолий Фёдорович", canvas.width / 2, canvas.height - 20);
    context.fillText("Ver 0.0.1", canvas.width / 2, canvas.height - 5);
// Game start code
}
function startGame() {
    gameStarted = true;
    clearCanvas();

    requestAnimationFrame(loop);

}

function complete() {
    clearCanvas();
    completed = true;

    context.font = '50px Lato';
    context.fillStyle = "#cc2366";
    context.textAlign = "center";
    context.fillText("Congrats! You Win!", canvas.width / 2, canvas.height / 2);
// Invitation to the game
    context.font = '20px Lato';
    context.fillText("Press Enter to play Again", canvas.width / 2, canvas.height / 2 + 50);
}

function reset() {
    player.x = 5;
    player.y = canvas.height - 25;
    player.grounded = true;
    player.velX = 0;
    player.velY = 0;
    completed = false;

    requestAnimationFrame(loop);
}

function loop() {
    clearCanvas();
    player.draw();
    goal.draw();
    draw_platform();
    green_grass();
    player.position = 'idle';
    // Jumping up
    if (keys[87] || keys[74]) {
        if (!player.jumping) {
            player.velY = -player.jumpStrenght * 2;
            player.jumping = true;
        }

    }
    // Move right
    if (keys[68]) {
        // console.log('right arrow');
        if (player.velX < player.speed) {
            player.position = 'right';
            player.velX++;
        }

    }
    // Move left
    if (keys[65]) {
        // console.log('left arrow');
        if (player.velX > -player.speed) {
            player.position = 'left';
            player.velX--;
        }

    }
    player.x += player.velX;
    player.velX *= friction;

    player.y += player.velY;
    player.velY += gravity;

    player.grounded = false;
    for (var i = 0; i < platforms.length; i++) {
        var direction = collisionCheck(player, platforms[i]);

        if (direction === 'left' || direction === 'right') {
            player.velX = 0;
        } else if (direction === 'top') {
            player.velY *= -1;
        } else if (direction === 'bottom') {
            player.jumping = false;
            player.grounded = true;
        }

    }

    if (player.grounded) {
        player.velY = 0;
    }

    /*    if (player.y >= canvas.height - player.height){
     player.y = canvas.height - player.height;
     player.jumping = false;
     }*/
    if (collisionCheck(player, goal)) {
        complete();
    }
    if (!completed) {
        requestAnimationFrame(loop);
    }

}

function collisionCheck(character, platform) {

    var vectorX = (character.x + (character.width / 2)) - (platform.x + (platform.width / 2));
    var vectorY = (character.y + (character.height / 2)) - (platform.y + (platform.height / 2));

    var halfWidths = (character.width / 2) + (platform.width / 2);
    var halfHeights = (character.height / 2) + (platform.height / 2);

    var collisionDirection = null;

    if (Math.abs(vectorX) < halfWidths && Math.abs(vectorY) < halfHeights) {
        console.log('Boomth');
        var offsetX = halfWidths - Math.abs(vectorX);
        var offsetY = halfHeights - Math.abs(vectorY);

        if (offsetX < offsetY) {

            if (vectorX > 0) {

                collisionDirection = 'left';
                character.x += offsetX;

            } else {

                collisionDirection = 'right';
                character.x -= offsetX;

            }

        } else {

            if (vectorY > 0) {

                collisionDirection = 'top';
                character.y += offsetY;

            } else {

                collisionDirection = 'bottom';
                character.y -= offsetY;

            }

        }

    }

    return collisionDirection;

}
// Drawing green grass
function green_grass() {
    context.lineWidth = 5;
    context.strokeStyle = '#90D030';
    for (var i = 0; i < platforms.length; i++) {
        context.strokeRect(platforms[i].x, platforms[i].y - 2, platforms[i].width, 5);
    }

}

// Platform rendering function
function draw_platform() {
    context.fillStyle = '#333333';

    for (var i = 0; i < platforms.length; i++) {
        context.fillRect(platforms[i].x, platforms[i].y, platforms[i].width, platforms[i].height);
    }
}

function clearCanvas() {
    context.clearRect(0, 0, canvas.width, canvas.height);
}












